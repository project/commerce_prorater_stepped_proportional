Commerce Prorater Stepped Proportional
======================================

This module provides a pro-rata plugin for Commerce Recurring which allows
proportional pro-rata prices at specific intervals in the billing period.

For example, with a billing period of 1 year, and 4 steps, a price of 100 would
be charged at:

- 0-3 months: 25
- 3-6 months: 50
- 6-9 months: 75
- 9-12 months: 100
